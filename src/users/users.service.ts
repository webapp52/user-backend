import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';

let users: User[] = [
  { id: 1, login: 'admin', name: 'Adminstrator', password: 'Pass@1234' },
  { id: 2, login: 'user1', name: 'User1', password: 'Pass@1234' },
  { id: 3, login: 'user2', name: 'User2', password: 'Pass@1234' },
];

let lastId = 4;

@Injectable()
export class UsersService {
  /* create(createUserDto: CreateUserDto) {
    const newUser = new User();
    newUser.id = lastId++;
    newUser.login = createUserDto.login;
    newUser.name = createUserDto.name;
    newUser.password = createUserDto.password;
    users.push(newUser);
    return newUser;
  } */

  create(createUserDto: CreateUserDto) {
    console.log({ ...createUserDto });
    const newUser: User = {
      id: lastId++,
      ...createUserDto,
    };
    users.push(newUser);
    return newUser;
  }

  findAll() {
    return users;
  }

  findOne(id: number) {
    const index = users.findIndex((user) => {
      return user.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    return users[index];
  }

  update(id: number, updateUserDto: UpdateUserDto) {
    const index = users.findIndex((user) => {
      return user.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }

    /* console.log(users[index]);
    console.log(updateUserDto); */
    const updateUser: User = {
      ...users[index],
      ...updateUserDto,
    };
    users[index] = updateUser;
    return updateUser;
  }

  remove(id: number) {
    const index = users.findIndex((user) => {
      return user.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    const deletedUser = users[index];
    users.splice(index, 1);
    return deletedUser;
  }

  reset() {
    users = [
      { id: 1, login: 'admin', name: 'Adminstrator', password: 'Pass@1234' },
      { id: 2, login: 'user1', name: 'User1', password: 'Pass@1234' },
      { id: 3, login: 'user2', name: 'User2', password: 'Pass@1234' },
    ];
    lastId = 4;
    return 'Reset';
  }
}
