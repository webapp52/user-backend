import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entities/product.entity';

let products: Product[] = [
  { id: 1, name: 'Pad Kra Pao', price: 45 },
  { id: 2, name: 'Kao kai jeaw', price: 35 },
  { id: 3, name: 'Pad tai', price: 45 },
];

let lastId = 4;

@Injectable()
export class ProductService {
  create(createProductDto: CreateProductDto) {
    console.log({ ...createProductDto });
    const newProduct: Product = {
      id: lastId++,
      ...createProductDto,
    };
    products.push(newProduct);
    return newProduct;
  }

  findAll() {
    return products;
  }

  findOne(id: number) {
    const index = products.findIndex((product) => {
      return product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    return products[index];
  }

  update(id: number, updateProductDto: UpdateProductDto) {
    const index = products.findIndex((product) => {
      return product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }

    /* console.log(Products[index]);
    console.log(updateProductDto); */
    const updateProduct: Product = {
      ...products[index],
      ...updateProductDto,
    };
    products[index] = updateProduct;
    return updateProduct;
  }

  remove(id: number) {
    const index = products.findIndex((Product) => {
      return Product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    const deletedProduct = products[index];
    products.splice(index, 1);
    return deletedProduct;
  }

  reset() {
    products = [
      { id: 1, name: 'Pad Kra Pao', price: 45 },
      { id: 2, name: 'Kao kai jeaw', price: 35 },
      { id: 3, name: 'Pad tai', price: 45 },
    ];
    lastId = 4;
    return 'Reset';
  }
}
